Coding Values
=============
These are the coding values of Laerdal Copenhagen. We believe that these values constitute the basis of good code. The value set is not a coding standard. Rather it is a vocabulary to talk sensible about the quality of each others code: "Hey dear colleague: do you think my class is *orthogonal* enough?"

The values are inherently mutually contradictory: Code can't be extremely decoupled and still be pragmatic. Ultimately that means that you need to use your common sense when balancing the values.

Values aren't absolute: You can choose to ignore a value if you think it makes sense. However expect to be asked for a very good explanation why you did it.

Clarity
-------
Produce code that the next developer - including yourself - will be able to understand. That means write code that exposes the intent of the code: What is the meaning of this class, this method, this field, this local variable, 

Orthogonality
-------------
Avoid muddling stuff up. If you have a class that translates strings and substitutes mold variables, why not break it in (at least) two? Then you can reuse the translator in programs that don't have a model and the substitutor can probably be used for something interesting. 

Orthogonal is important on package, class, as well as method level. Don't make kitchen sink packages that can do a million of things poorly instead of a single thing that is both understandable and tes$table.

Shared
------
Make sure somebody else also understands what you are coding. Offer up your code to review, pair, triple, n-tuple program, brag about you brilliant ideas. Give constructive feedback and take it yourself. If you feel the need to excuse your code when you show it off, maybe you you should collaborefactor it instead.

Living Code
-----------
Something strange happens to code that lives in a quite corner of a project, untouched by human hands for ages. It seems to develop flaws and the intent of the code starts to evaporate. Dust the code off once in a while. Make sure it still compiles and passes the tests and brush up on the inner workings. Maybe there's even a gem hiding you can use for something else!

Tested
------
Test stuff! Deliver code that works and a really compelling argument for that is a suite of tests. The usual arguments for unit tests apply: The code gets tested and it's safer to do refactoring. Furthermore, unit tests can force you to think a bit deeper about how you want the code you write to behave. Also, unit tests kind of encourages decoupling.

Decoupled
---------
Don't tie things too tightly together. If you design an api don't assume too much of the api clients. Expose the functionality of the api, not how you think it will be used. Don't worry too much about having a bare bones api because you can always add an adaptor layer on top that dumbs things down in a way that suits a class of clients.

As a client of an api, external or internal, don't tie yourself too closely to the design. At some point, the api will become obsolete and in need of replacement. Ideally you've designed the code so there is a clean client specific layer towards the api so the changes you need are really isolated. More often, that's not the case but at least, plan ahead for retirement.

The same goes for code inside you project. The less each part (packages, classes, methods) know about each other, the better.

Pragmatic
---------
Resist the temptation to over-engineer. Don't build a complex system for solving a simple problem. Use the abstractions available in the programming language but remember that there is a cognitive cost to overly abstract code.

Boy Scout
---------
Leave everything better than when you came. If you spend half a day getting to an understanding of a convoluted method, leave a comment to the next programmer. Even better, write a few test cases and maybe even refactor it!

Are the line endings broken? Mixed tabs and spaces? Clean it up if you are working on the file as well.

Be a Good Colleague
-------------------
Be constructive. Share. Reflect. Improve yourself. Accept that while you are a genius, the next person might have thought of something you haven't. There is no right way to make programs (but a whole lot of wrong ways!) and your colleagues way to solve a problem may be radically different compared to yours but still pretty darn good. Respect that! Learn from it but also share how you would have solved the problem and what you see as problematic points.

Remember that there is a lot of pride having produced something so involve the original author(s) before you radically change things. They can probably get you up to speed faster as well.




 - code values
   - robust
   - testable
   - throw-outable
   - decoupled
     - responsibilities
     - from third party tech
     - sensible thin interfaces
     - functional
   - single responsibility
     - methods, classes
   - max 7 (4?) concurrent "items" in mind
     - short methods
     - few and/or orthogonal fields

2017-11-20
==========
 - code values
   - don't comment out code instead of deleting it
   - intent should be obvious in the code
   - peer review to uphold values
   - make meaningful commits
   - share
   - error handling
     - don't fail silently 
 - involve peers earlier
   - complicated coding decisions should be taken in a group

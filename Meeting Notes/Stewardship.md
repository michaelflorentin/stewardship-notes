Stewardship
===========
Needs:
 - Responsibility
 - Respect
 - Knowledge sharing

Manage Authority
 - "experts" 
 - Resist consistency?

Manage Impact:
 - 

Knowledge Management
 - knowledge about tools, code is lost
 - Organizational, Ecological

Culture change
 - Leadership (vision, values) + Management (tool, process) + Power
 - Culture for esim department?

Vision:
 - To efficiently create and maintain high quality software products. 
 - Reduce friction.

Narratives?

Motivation:
 + play, purpose, potential (total motivation)
 - emotional pressure, economic pressure, inertia

Retro:
  1. Play: What did I learn this week? 
  2. Purpose: What impact did I have this week? 
  3. Potential: What do I want to learn next week?

Change Lifetime Management
--------------------------
Consider impact on others:
 - Technology
 - Processes
 - Power

Plan for retirement
 - Invasive
 - Replacement


Skunk Management
----------------
Foster ideas.
However move into 

Cut the cake (organization)
---------------------------

1: people/artifacts

Artifacts:
 - tools
 - processes
 - values
 - culture
 - power
 - relations
 - needs
 - identity

2: formal/informal

3: predictable/creative


Notes 2017-11-10
================
 - learn from concrete tools/code 
    - speaksite stewardship
        - map activities, roles
 - what would good stewardship be for this tiny thing
 - code dependency cleanup
 - code convention
 - Anne Gerd

Work items
 - action manager (π)
 - stewardship: how to ensure code quality
 - ACLS hacker team
 - 1.6 unity team

For 2017-11-15
==============
 - speak site (Brennan?)
 - 1.6 / 2.0+ tracks
   - knowledge sharing between tracks
   - keep resources on 2.0+
   - ensure code quality in 1.6
     - separation between Unity and business logic
     - thin interface between front end and back end


 2017-11-15
 ==========      
 - values & advocates
   - localizatable solutions
 - code dojo / talks
   - pull request, review workflow (gabriel)
   - unit test(ability)
   - git 
   - replay

work items:
 - form values task force
   - pete, hanehøj, mikkel + stewardship team
 - topics for dojo?

For 2017-11-20
==============
 - Code values vs code standard
   - less rigid
   - encourages/requires common sense/reflection
     - self contradictory
   - individual values can be down-prioritized if there is a really good reason
 - code values
   - readable
   - robust
   - testable
   - throw-outable
   - decoupled
     - responsibilities
     - from third party tech
     - sensible thin interfaces
     - functional
   - single responsibility
     - methods, classes
   - max 7 (4?) concurrent "items" in mind
     - short methods
     - few and/or orthogonal fields

2017-11-20
==========
 - code values
   - don't comment out code instead of deleting it
   - intent should be obvious in the code
   - peer review to uphold values
   - make meaningful commits
   - share
   - error handling
     - don't fail silently 
 - involve peers earlier
   - complicated coding decisions should be taken in a group

actions:
 - collect values
 - start document

For 2017-11-23
==============
 - meeting culture?
   - shorter meeting
   - don't start waiting for ppl/tech
 - dojos
   - unity (Hanehøj)
   - pragmatic fp
 - code archeology (Hans Henrik)


2017-11-23
==========
 - meetings
   - dont make expanding meetings into a meting culture
   - pomodoro
   - meeting cost timer
   - meeting free days
 - dojo
   - hows the difference from a class?
   - refactoring
     - for 1.6?
   - dojo size e.g. 6: 3 "competing" pairs
   - external problem owner
 - masterclasses
   - unity
     - UI, programmers, designers
   - pragmatic fp

For next time
=============
 - code archeology (Hans Henrik)
